--  Copyright 2016 Hugo Dupraz
--
--  This file is part of Virator.
--
--  Virator is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  Virator is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with Virator.  If not, see <http://www.gnu.org/licenses/>.

function love.load()
    -- Check love version
    if love._version_major == nil
            or love._version_major < 0
            or (love._version_major == 0 and love._version_minor < 9) then
        error("This version of Löve is outdated, please install 0.9 or higher.")
    end

    love.graphics.clear()
    love.graphics.present()

    -- Preload stuff here
    require "game" -- define the gameplay and big function
    require "virus" -- define virus
    require "cellule" -- define cellules
    require "graphics" -- load sprites

    game_load() -- Init the world

    -- Setup callbacks
    love.draw = game_draw
    love.keypressed = game_keypressed
    love.update = game_update
end
