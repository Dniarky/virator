  function load_graphics()
    sprites = {}
    sprites["virusparticle"] = {}
    sprites["virusparticle"].image = love.graphics.newImage('graphics/virusparticle.png')
    local tileW, tileH = 4,4
    local tilesetW, tilesetH = sprites["virusparticle"].image:getWidth(), sprites["virusparticle"].image:getHeight()
    sprites["virusparticle"].tileW, sprites["virusparticle"].tileH = 4,4
    sprites["virusparticle"].tilesetW, sprites["virusparticle"].tilesetH = sprites["virusparticle"].image:getWidth(), sprites["virusparticle"].image:getHeight()
    sprites["virusparticle"][1] = love.graphics.newQuad(0, 0, tileW, tileH, tilesetW    , tilesetH)

    sprites["badasscellule"] = {}
    sprites["badasscellule"].image = love.graphics.newImage('graphics/badasscellule.png')
    local tileW, tileH = 256,256
    local tilesetW, tilesetH = sprites["badasscellule"].image:getWidth(), sprites["badasscellule"].image:getHeight()
    sprites["badasscellule"].tileW, sprites["badasscellule"].tileH = 256,256
    sprites["badasscellule"].tilesetW, sprites["badasscellule"].tilesetH = sprites["badasscellule"].image:getWidth(), sprites["badasscellule"].image:getHeight()
    sprites["badasscellule"][1] = love.graphics.newQuad(0, 0, tileW, tileH, tilesetW    , tilesetH)

    sprites["badasscellulecalm"] = {}
    sprites["badasscellulecalm"].image = love.graphics.newImage('graphics/badasscellulecalm.png')
    local tileW, tileH = 256,256
    local tilesetW, tilesetH = sprites["badasscellulecalm"].image:getWidth(), sprites["badasscellulecalm"].image:getHeight()
    sprites["badasscellulecalm"].tileW, sprites["badasscellulecalm"].tileH = 256,256
    sprites["badasscellulecalm"].tilesetW, sprites["badasscellulecalm"].tilesetH = sprites["badasscellulecalm"].image:getWidth(), sprites["badasscellulecalm"].image:getHeight()
    sprites["badasscellulecalm"][1] = love.graphics.newQuad(0, 0, tileW, tileH, tilesetW    , tilesetH)

    sprites["globule"] = {}
    sprites["globule"].image = love.graphics.newImage('graphics/globule.png')
    local tileW, tileH = 256,256
    local tilesetW, tilesetH = sprites["globule"].image:getWidth(), sprites["globule"].image:getHeight()
    sprites["globule"].tileW, sprites["globule"].tileH = 256,256
    sprites["globule"].tilesetW, sprites["globule"].tilesetH = sprites["globule"].image:getWidth(), sprites["globule"].image:getHeight()
    sprites["globule"][1] = love.graphics.newQuad(0, 0, tileW, tileH, tilesetW    , tilesetH)


    sprites["maccro"] = {}
    sprites["maccro"].image = love.graphics.newImage('graphics/maccro.png')
    local tileW, tileH = 256,256
    local tilesetW, tilesetH = sprites["maccro"].image:getWidth(), sprites["maccro"].image:getHeight()
    sprites["maccro"].tileW, sprites["maccro"].tileH = 256,256
    sprites["maccro"].tilesetW, sprites["maccro"].tilesetH = sprites["maccro"].image:getWidth(), sprites["maccro"].image:getHeight()
    sprites["maccro"][1] = love.graphics.newQuad(0, 0, tileW, tileH, tilesetW    , tilesetH)
    sprites["maccro"][2] = love.graphics.newQuad(1*tileW, 0, tileW, tileH, tilesetW    , tilesetH)
    sprites["maccro"][3] = love.graphics.newQuad(2*tileW, 0, tileW, tileH, tilesetW    , tilesetH)
    sprites["maccro"][4] = love.graphics.newQuad(3*tileW, 0, tileW, tileH, tilesetW    , tilesetH)
    sprites["maccro"][5] = love.graphics.newQuad(4*tileW, 0, tileW, tileH, tilesetW    , tilesetH)



    particules = {x = 200, y = 200, numbername = 1}
    particules["list"] = {}
    function particules:new(o)
        o = o or {}
        setmetatable(o,self)
        self.__index = self
        particules["list"][particules.numbername] = o
        particules.numbername = particules.numbername + 1
        o.sys = love.graphics.newParticleSystem(sprites["virusparticle"].image, 300)
        return o
    end
end
