--  Copyright 2016 Hugo Dupraz
--
--  This file is part of Virator.
--
--  Virator is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  Virator is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with Virator.  If not, see <http://www.gnu.org/licenses/>.


Virus = {x = 300, y = 400, size = 20, hp = 1, food = 0, numbername = 1, speed = 100, dad = nil, isalive = false}
Virus["list"] = {}


function Virus:new(o)
   o = o or {}
   setmetatable(o,self)
   self.__index = self
   Virus["list"][Virus.numbername] = o
   Virus.numbername = Virus.numbername + 1
   o.speed = 100
   o.name = Virus.numbername
   o.physicalbody = love.physics.newBody(world, o.x, o.y, "dynamic")
   o.shape = love.physics.newCircleShape(o.size) 
   o.fixture = love.physics.newFixture(o.physicalbody, o.shape, 1)
   o.fixture:setUserData(self)
   o.fixture:setRestitution(0.09)
   o.fixture:setDensity(1)
   o.fixture:setFriction(0.2)
   o.hp = 1
   o.isalive = true
   return o
end

function Virus:move(dt)
    local xf, yf = self.wantR - self.wantL, self.wantD - self.wantH
    self.physicalbody:applyForce(xf*65*self.speed,yf*65*self.speed)
    xv, yv = self.physicalbody:getLinearVelocity()
    self.physicalbody:applyForce(-xv*10,-yv*10)
end

function Virus:mute(dt)
    if self.dad then
        if self.dad.isalive then
            local xf, yf = (self.dad.physicalbody:getX() - self.physicalbody:getX())/math.max(100,math.abs(self.dad.physicalbody:getX() - self.physicalbody:getX())),(self.dad.physicalbody:getY() - self.physicalbody:getY())/math.max(100,math.abs(self.dad.physicalbody:getY() - self.physicalbody:getY()))
            self.physicalbody:applyForce(-xf*65*self.speed,-yf*65*self.speed)
        end
    end
end

function Virus:eat(dt)
    toeat, dmin = nil, 1000
    for k,l in pairs(Cellule["list"]) do
        local dx, dy = math.abs(self.physicalbody:getX() - l.physicalbody:getX()), math.abs(self.physicalbody:getY() - l.physicalbody:getY())
        local d = dx*dx + dy*dy
        d = math.sqrt(d) - l.size
        if d < 80 and d < dmin then
            toeat = l
        end
    end
    if toeat then
        if self.eating then
        self.eating = self.eating + dt
        else self.eating = 0
        end
        toeat.hp = toeat.hp - 1 
        toeat.size = math.max(20,(toeat.hp + 29.25)/(toeat.hp+30)*toeat.size)
        toeat.shape = love.physics.newCircleShape(toeat.size) 
        toeat.fixture:destroy()
        toeat.fixture = love.physics.newFixture(toeat.physicalbody, toeat.shape, 1)
        toeat.fixture:setUserData(self)
        toeat.fixture:setRestitution(0.09)
        toeat.fixture:setCategory(2)
        toeat.fixture:setDensity(1)
        toeat.fixture:setFriction(0.2)
        self.eatingfood = false
        if toeat.isfood then
            self.eatingfood = true
            self.tx, self.ty = (toeat.physicalbody:getX() - self.physicalbody:getX()) ,(toeat.physicalbody:getY() - self.physicalbody:getY())
        self.food = self.food + 1
        end
    elseif self.eating then
    self.eating = false
    end
end

function Virus:dessine()
    if not(self.eating) then
    love.graphics.setColor(20 + math.min(100,self.food),190,00 + self.speed)
    elseif self.eatingfood then
        love.graphics.setColor(20 + math.min(100,self.food) ,190,00 + self.speed + 30*math.abs(math.sin(3*self.eating)))
    else
        love.graphics.setColor(50 + math.min(100,self.food) + 100*math.abs(math.sin(3*self.eating)) ,190,00 + self.speed )
    end
    if self.eating then
    if self.eatingfood then
        local d = math.sqrt(self.tx*self.tx + self.ty*self.ty)
        local xta, yta = self.tx / d, self.ty / d
        if xta > 0.001 then
        a = math.atan(yta/xta)
        elseif xta < 0.001 then
        a = math.atan(yta/xta) + math.pi
        elseif yta > 0 then
        a = math.pi/2
        elseif yta < 0 then
        a = -math.pi/2
        end
        local xrel, yrel = -math.cos(a), -math.sin(a)
        local xm, ym = 128*(self.size / 80)*xrel - 128*(self.size / 80)*yrel, 128*(self.size / 80)*yrel + 128*(self.size / 80)*xrel 
        if math.cos(self.eating*7) > 0.8 then
        love.graphics.draw(sprites["maccro"].image,sprites["maccro"][5], self.physicalbody:getX() + xm, self.physicalbody:getY() + ym, a, self.size / 80, self.size / 80)
    elseif math.cos(self.eating*7) > 0.65 then
        love.graphics.draw(sprites["maccro"].image,sprites["maccro"][4], self.physicalbody:getX() + xm, self.physicalbody:getY() + ym, a, self.size / 80, self.size / 80)
    elseif math.cos(self.eating*7) > 0.4 then
        love.graphics.draw(sprites["maccro"].image,sprites["maccro"][3], self.physicalbody:getX() + xm, self.physicalbody:getY() + ym, a, self.size / 80, self.size / 80)
    elseif math.cos(self.eating*7) > 0.0 then
        love.graphics.draw(sprites["maccro"].image,sprites["maccro"][2], self.physicalbody:getX() + xm, self.physicalbody:getY() + ym, a, self.size / 80, self.size / 80)
    else
        love.graphics.draw(sprites["maccro"].image,sprites["maccro"][1], self.physicalbody:getX() + xm, self.physicalbody:getY() + ym, a, self.size / 80, self.size / 80)
    end
else
    if math.sin(self.eating*17)>0 then
        love.graphics.draw(sprites["badasscellule"].image,sprites["badasscellule"][1], self.physicalbody:getX()-(128*(self.size / 80)), self.physicalbody:getY()-(128*(self.size / 80)), 0, self.size / 80, self.size / 80)
    end
    end
    end
    love.graphics.circle("fill", self.physicalbody:getX(), self.physicalbody:getY(), self.shape:getRadius())
end

function Virus:dead()
    if self.hp < 1 then
        local part = particules:new{x=self.physicalbody:getX(), y = self.physicalbody:getY()} 
        part.sys:setParticleLifetime(0.8,1.2)
        part.sys:setEmissionRate(200)
        part.sys:setSpeed(0)
        part.sys:setEmitterLifetime(0.2)
        part.sys:setAreaSpread("normal", 20, 20)
        part.sys:setColors(50,205,50,255, 100, 200, 100, 0)
        part.sys:setLinearAcceleration(-30,-30,30,30)
        part.sys:start()
        part.sys:emit(130)
        self.isalive = false
        return true
    else
        return false
    end
end

function Virus:reproduce()
    if self.food >= 180 then
        local fils = self:new{x = self.physicalbody:getX() + self.size*1.5*math.cos(math.random(19)), y = self.physicalbody:getY() + self.size*1.5*math.sin(math.random(19)), size = math.max(5,self.size + math.random(10)/5 - math.random(10)/5), speed = self.speed + math.random(60)/10 - math.random(60)/10 }
        fils.joint = love.physics.newRopeJoint( fils.physicalbody, self.physicalbody, fils.physicalbody:getX(), fils.physicalbody:getY(), self.physicalbody:getX(), self.physicalbody:getY(), 110, true )
        fils.dad = self
        self.food = self.food - 100
        self.speed = self.speed * 0.9
    end
end
