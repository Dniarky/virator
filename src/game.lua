--  Copyright 2016 Hugo Dupraz
--
--  This file is part of Virator.
--
--  Virator is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  Virator is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with Virator.  If not, see <http://www.gnu.org/licenses/>.

function game_load()
    load_graphics()
    
    world = love.physics.newWorld(0,0, true);
    -- Invisible Wall
    LeftWall = {}
    LeftWall.physicalbody = love.physics.newBody(world, -260, love.graphics.getHeight()/2, "static")
    LeftWall.shape = love.physics.newRectangleShape(500,love.graphics.getHeight() + 200) 
    LeftWall.fixture = love.physics.newFixture(LeftWall.physicalbody, LeftWall.shape, 1)
    LeftWall.fixture:setRestitution(0.01)
    LeftWall.fixture:setFriction(0.1)
    LeftWall.fixture:setMask(2)
    RightWall = {}
    RightWall.physicalbody = love.physics.newBody(world, love.graphics.getWidth()+260, love.graphics.getHeight()/2, "static")
    RightWall.shape = love.physics.newRectangleShape(500,love.graphics.getHeight() + 200) 
    RightWall.fixture = love.physics.newFixture(RightWall.physicalbody, RightWall.shape, 1)
    RightWall.fixture:setRestitution(0.01)
    RightWall.fixture:setFriction(0.1)
    RightWall.fixture:setMask(2)
    UpWall = {}
    UpWall.physicalbody = love.physics.newBody(world,love.graphics.getWidth()/2, -260, "static")
    UpWall.shape = love.physics.newRectangleShape(love.graphics.getWidth() + 200, 500) 
    UpWall.fixture = love.physics.newFixture(UpWall.physicalbody, UpWall.shape, 1)
    UpWall.fixture:setRestitution(0.01)
    UpWall.fixture:setFriction(0.1)
    UpWall.fixture:setMask(2)
    DownWall = {}
    DownWall.physicalbody = love.physics.newBody(world,love.graphics.getWidth()/2,love.graphics.getHeight() + 260, "static")
    DownWall.shape = love.physics.newRectangleShape(love.graphics.getWidth() + 200, 500) 
    DownWall.fixture = love.physics.newFixture(DownWall.physicalbody, DownWall.shape, 1)
    DownWall.fixture:setRestitution(0.01)
    DownWall.fixture:setFriction(0.1)
    DownWall.fixture:setMask(2)
    Event = {}
    Event["mode"] = "menu"
    Event["menu"] = "title"
    Event["ingame"] = false
    Event["campaign"] = 1
end

function initgame(pera, perb, perc, perd, tb)
    Event["ingame"] = true
    time = 0
    spawnchance = 8.5
    tailleboss = tb
    InfectedCellule = 0
    PerBase, PerSpeed, PerMax = pera, perb, perc
    PerRal = perd
    gameended = false
    noboss = true
    Virus:new()
    numberVirus = 1
    Cellule:new{ x = math.random(200) + 500, y = math.random(200) + 300}
    if Event["mode"] == "campaign" then
        Event["timemax"] = 180
        Event["numbercel"] = 180
    elseif Event["mode"] == "endless" then
        Event["timemax"] = 100000
        Event["numbercel"] = 100000
    end
end

function finishgame(victoryornot)
    if victoryornot == "loosetimeout" then
        if Event["mode"] == "endless" then Event["mode"] = "menu" end
        if Event["campaign"] == 3 then Event["campaign"] = "l2" end
        if Event["campaign"] == 5 then Event["campaign"] = "l4" end
        if Event["campaign"] == 7 then Event["campaign"] = "l6" end
    elseif victoryornot == "loosenovir" then
        if Event["mode"] == "endless" then Event["mode"] = "menu" end
        if Event["campaign"] == 3 then Event["campaign"] = "l2" end
        if Event["campaign"] == 5 then Event["campaign"] = "l4" end
        if Event["campaign"] == 7 then Event["campaign"] = "l6" end
    elseif victoryornot == "win" then
        if Event["mode"] == "endless" then Event["mode"] = "menu" end
        if Event["campaign"] == 3 then Event["campaign"] = 4 end
        if Event["campaign"] == 5 then Event["campaign"] = 6 end
        if Event["campaign"] == 7 then Event["campaign"] = 8 end
    end
    for i,j in pairs(Virus["list"]) do 
        j.physicalbody:destroy()
        Virus["list"][i] = nil
    end
    for i,j in pairs(Cellule["list"]) do 
        j.fixture:destroy()
        Cellule["list"][i] = nil
    end
    time = nil
    spawnchance = nil
    InfectedCellule = nil
    numberVirus = nil
    Event["ingame"] = false
end

function game_update(dt)
    function love.keypressed(key, isrepeat)
        if key == 'escape' then
            love.event.push('quit')
        end
    end
    if Event["mode"] == "menu" then
        if Event["menu"] == "title" then
            if love.keyboard.isDown('1') then
                    Event["mode"] = "campaign"
                    Event["campaign"] = 2
            end
            if love.keyboard.isDown('2') then
                    Event["mode"] = "endless"
                    initgame(90, 200, 80, 0, 300) -- endlessoptions
            end
        end
    elseif Event["mode"] == "campaign" then
        if Event["campaign"] == 1 then
            if love.keyboard.isDown(' ') then
                Event["campaign"] = 2
            end
        elseif (Event["campaign"] == 2) or (Event["campaign"] == "l2") then
            if love.keyboard.isDown(' ') then
                Event["campaign"] = 3
                initgame(90, 80, 10, 0, 200) -- campaign easy one
            end
        elseif (Event["campaign"] == 4) or (Event["campaign"] == "l4") then
            if love.keyboard.isDown(' ') then
                Event["campaign"] = 5
                initgame(85, 60, 30, 0, 300) -- campaign vaccinated one
            end
        elseif (Event["campaign"] == 6) or (Event["campaign"] == "l6") then
            if love.keyboard.isDown(' ') then
                Event["campaign"] = 7
                initgame(86, 60, 47, 0, 400) -- campaign boss one
            end
        end
    end

    if Event["ingame"] == true then
        Virus.wantR, Virus.wantE, Virus.wantL, Virus.wantD, Virus.wantH, Virus.wantM = 0, true, 0, 0, 0, false
        if love.keyboard.isDown('w') then
                Virus.wantH = 1
        end
        if love.keyboard.isDown('s') then
                Virus.wantD = 1
        end
        if love.keyboard.isDown('a') then
                Virus.wantL = 1
        end
        if love.keyboard.isDown('d') then
                Virus.wantR = 1
        end
        if love.keyboard.isDown(' ') then
                Virus.wantM = true 
        end

        time = time + dt
        if time > Event["timemax"] then
            gameended = "loosetimeout"
        end

        spawnchance = spawnchance + dt*math.sqrt(time+30)/10
        local joueur = false
        for i,j in pairs(Virus["list"]) do 
            joueur = true
            j:move(dt)
            if Virus.wantE then
                j:eat(dt)
            end
            if Virus.wantM then
                j:mute(dt)
            end
            j:reproduce()
            if j:dead() then
                j.physicalbody:destroy()
                Virus["list"][i] = nil
            end
        end
        if not(joueur) then
            gameended = "loosetimeout"
        end

        for i,j in pairs(Cellule["list"]) do 
            j:move(dt)
            if j.killingon then
                j:kill(dt)
            end
            if j.ral then
                j:ral(dt)
            end
            if j:dead() then
                if j.isboss then
                    InfectedCellule = InfectedCellule + 1
                else
                    InfectedCellule = InfectedCellule + 1
                end
                j.fixture:destroy()
                Cellule["list"][i] = nil
            end
        end
        if InfectedCellule > Event["numbercel"] then
            gameended = "win"
        end

        if math.random(10) < spawnchance then
            spawnchance = spawnchance - 0.75
            local angle = math.random(19)
            local bisangle = math.random(100)/300 + angle
            if math.random(100) > (PerBase - (time/(time + PerSpeed))*PerMax) then
                    if math.random(100) > PerRal then
                    CelluleBadass:new{x = 1300*math.cos(angle) + love.graphics.getWidth()/2, y = 1300*math.sin(angle) + love.graphics.getHeight()/2, xv = -1000 * math.cos(bisangle), yv = -1000 * math.sin(bisangle)}
                    else
                    CelluleChiante:new{x = 1300*math.cos(angle) + love.graphics.getWidth()/2, y = 1300*math.sin(angle) + love.graphics.getHeight()/2, xv = -400 * math.cos(bisangle), yv = -400 * math.sin(bisangle)}
                    end
            else
                if math.random(100) < 90 then
                    Cellule:new{x = 1300*math.cos(angle) + love.graphics.getWidth()/2, y = 1300*math.sin(angle) + love.graphics.getHeight()/2, xv = -1500 * math.cos(bisangle), yv = -1500 * math.sin(bisangle)}
                else
                    Cellule:new{x = 1300*math.cos(angle) + love.graphics.getWidth()/2, y = 1300*math.sin(angle) + love.graphics.getHeight()/2, xv = -6000 * math.cos(bisangle), yv = -6000 * math.sin(bisangle), size = 180, hp = 780}
                end
            end
        end

        if time > 130 and noboss then
            Celluletruelastboss:new{ x = math.random(200) - 1000, y = math.random(200) + 300, size = tailleboss, hp = tailleboss*tailleboss/4}
            noboss = false
        end
        world:update(dt)
        for i,j in pairs(particules["list"]) do 
            j.sys:update(dt)
        end

        if gameended then
            finishgame(gameended)
        end
    end
end

function game_draw()
        
    love.graphics.push()

    --Background
        love.graphics.setColor(230, 50, 30)
        love.graphics.rectangle("fill",0,0,love.graphics.getWidth(),love.graphics.getHeight())
        love.graphics.setColor(30, 250, 30)
    if Event["mode"] == "menu" then
        if Event["menu"] == "title" then
            love.graphics.printf("Virator", love.graphics.getWidth()*4/12, love.graphics.getHeight()*2/18, 925, "left", 0, 10, 10)
            love.graphics.printf("Press '1':   New game", love.graphics.getWidth()*4/12, love.graphics.getHeight()*9/18, 925, "left", 0, 3, 3)
            love.graphics.printf("Press '2':  Endless game", love.graphics.getWidth()*4/12, love.graphics.getHeight()*11/18, 925, "left", 0, 3, 3)
        end
    elseif Event["mode"] == "campaign" then
        if Event["campaign"] == 1 then
            love.graphics.printf("Space to continue", love.graphics.getWidth()*4/12, love.graphics.getHeight()*15/18, 925, "left", 0, 3, 3)
        elseif Event["campaign"] == 2 then
            love.graphics.printf("In 2042, after the apocalyptic economic crisis, political extremists were rising. \nAmong them, the infamous RED, the neo-extremist-eco-communist,\nwant death of all human to restore earth health.", love.graphics.getWidth()*1/12, love.graphics.getHeight()*3/18, 925, "left", 0, 3, 3)
            love.graphics.printf("To achieve their goals, the RED create you!\n You are the deadly virus Virator. \nYour quest begin here, infecting some random american.", love.graphics.getWidth()*1/12, love.graphics.getHeight()*7/18, 925, "left", 0, 3, 3)
            love.graphics.printf("Space to continue", love.graphics.getWidth()*4/12, love.graphics.getHeight()*15/18, 925, "left", 0, 3, 3)
        elseif Event["campaign"] == "l2" then
            love.graphics.printf("You didn't infect enough cells in time.", love.graphics.getWidth()*1/12, love.graphics.getHeight()*3/18, 925, "left", 0, 3, 3)
            love.graphics.printf("Space to retry", love.graphics.getWidth()*4/12, love.graphics.getHeight()*15/18, 925, "left", 0, 3, 3)
        elseif Event["campaign"] == 4 then
            love.graphics.printf("It's a first victory, but after your first infection, \nScientists made some vaccines to fight against you. \nThey where surprisingly well administered to all american \ndue to the past reform of health care, thanks Obama!", love.graphics.getWidth()*1/12, love.graphics.getHeight()*3/18, 925, "left", 0, 3, 3)
            love.graphics.printf("Space to continue", love.graphics.getWidth()*4/12, love.graphics.getHeight()*15/18, 925, "left", 0, 3, 3)
        elseif Event["campaign"] == "l4" then
            love.graphics.printf("Fuck these vaccines, thanks Obama!", love.graphics.getWidth()*1/12, love.graphics.getHeight()*3/18, 925, "left", 0, 3, 3)
            love.graphics.printf("Space to retry", love.graphics.getWidth()*4/12, love.graphics.getHeight()*15/18, 925, "left", 0, 3, 3)
        elseif Event["campaign"] == 6 then
            love.graphics.printf("After some more research, the scientist found out that\n moar zucchinni could get the good vitamin to slow you down!", love.graphics.getWidth()*1/12, love.graphics.getHeight()*3/18, 925, "left", 0, 3, 3)
        elseif Event["campaign"] == "l6" then
            love.graphics.printf("You nearly destroy the human race, but not this time.", love.graphics.getWidth()*1/12, love.graphics.getHeight()*3/18, 925, "left", 0, 3, 3)
            love.graphics.printf("Space to retry", love.graphics.getWidth()*4/12, love.graphics.getHeight()*15/18, 925, "left", 0, 3, 3)
        end
    end

    if Event["ingame"] then
        love.graphics.setColor(230 - math.min(165, InfectedCellule/3), 50 + math.min(195, InfectedCellule/3),30)
        love.graphics.rectangle("fill",0,0,love.graphics.getWidth(),love.graphics.getHeight())
        for i,j in pairs(Cellule["list"]) do 
            j:dessine()
        end
        for i,j in pairs(Virus["list"]) do 
            j:dessine()
        end
        for i,j in pairs(particules["list"]) do 
            love.graphics.draw(j.sys, j.x, j.y)
        end

        if (Event["timemax"] < 1000) and (Event["numbercel"] < 1000) then
        love.graphics.setColor(20,250,50)
        minleft = math.floor((180 - time)/60)
        secleft = 60 - time + 60*(math.floor(time/60))
        love.graphics.printf(180 - InfectedCellule .. "\n" .. minleft .. ":" .. secleft  , love.graphics.getWidth()*11/12, love.graphics.getHeight()*1/18, 925, "left", 0, 3, 3)
        else
        love.graphics.setColor(20,250,50)
        minleft = math.floor((time)/60)
        secleft = time - 60*(math.floor(time/60))
        love.graphics.printf(InfectedCellule .. "\n" .. minleft .. ":" .. secleft  , love.graphics.getWidth()*11/12, love.graphics.getHeight()*1/18, 925, "left", 0, 3, 3)
        end
    end
    love.graphics.pop()
end
