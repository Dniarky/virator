--  Copyright 2016 Hugo Dupraz
--
--  This file is part of Virator.
--
--  Virator is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  Virator is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with Virator.  If not, see <http://www.gnu.org/licenses/>.


Cellule = {x = 200, y = 200, size = 90, hp = 220, numbername = 1, isfood = true}
Cellule["list"] = {}


function Cellule:new(o)
   o = o or {}
   setmetatable(o,self)
   self.__index = self
   Cellule["list"][Cellule.numbername] = o
   Cellule.numbername = Cellule.numbername + 1
   o.name = Cellule.numbername
   o.physicalbody = love.physics.newBody(world, o.x, o.y, "dynamic")
   o.shape = love.physics.newCircleShape(o.size) 
   o.fixture = love.physics.newFixture(o.physicalbody, o.shape, 1)
   o.fixture:setUserData(self)
   o.fixture:setRestitution(0.09)
   o.fixture:setCategory(2)
   o.fixture:setDensity(1)
   o.fixture:setFriction(0.2)
   if o.xv and o.yv then
        o.physicalbody:applyLinearImpulse(2*o.xv, 2*o.yv)
   end
   return o
end

function Cellule:move(dt)
    self.wantR = math.random(100)/100
    self.wantL = math.random(100)/100
    self.wantD = math.random(100)/100
    self.wantH = math.random(100)/100
    local xf, yf = self.wantR*dt - self.wantL*dt, self.wantD*dt - self.wantH*dt
    self.physicalbody:applyForce(xf*50000,yf*50000)
end

function Cellule:dessine()
    love.graphics.setColor(148+64-math.min(64,self.hp/5),10+100-math.min(100,self.hp/3),35+100-math.min(100,self.hp/3))
    love.graphics.draw(sprites["globule"].image,sprites["globule"][1], self.physicalbody:getX()-(128*(self.size / 80)), self.physicalbody:getY()-(128*(self.size / 80)), 0, self.size / 80, self.size / 80)
end

function Cellule:dead()
    if self.hp < 1 then
        return true
    else
        return false
    end
end

CelluleBadass = {size = 80, hp = 110, killingon = 0, isfood = false}
setmetatable(CelluleBadass, Cellule)
Cellule.__index = CelluleBadass

function CelluleBadass:move(dt)
    local n = 0
    self.wantR, self.wantL, self.wantD, self.wantH = 0,0,0,0
    for k,l in pairs(Virus["list"]) do
    self.wantR = self.wantR + math.min(1,math.abs(math.max(0,l.physicalbody:getX()-self.physicalbody:getX())))
    self.wantL = self.wantL + math.min(1,math.abs(math.min(0,l.physicalbody:getX()-self.physicalbody:getX())))
    self.wantD = self.wantD + math.min(1,math.abs(math.max(0,l.physicalbody:getY()-self.physicalbody:getY())))
    self.wantH = self.wantH + math.min(1,math.abs(math.min(0,l.physicalbody:getY()-self.physicalbody:getY())))
    n = n + 1
    end
    if n == 0 then n = 1 end
    self.wantR = self.wantR/n
    self.wantL = self.wantL/n
    self.wantD = self.wantD/n
    self.wantH = self.wantH/n
    local xf, yf = self.wantR*dt - self.wantL*dt, self.wantD*dt - self.wantH*dt
    self.physicalbody:applyForce(xf*50000*math.sqrt(time+100)/10,yf*50000*math.sqrt(time+100)/10)
    xv, yv = self.physicalbody:getLinearVelocity()
    self.physicalbody:applyForce(-xv*8,-yv*8)
end

function CelluleBadass:dessine()
    local R,G,B = 0,0,0
    if math.cos(self.killingon) > 0 then
        R,G,B = 250,250,0
    elseif math.cos(self.killingon) > -0.4 and math.sin(self.killingon) < 0 then 
        R,G,B = 220,220,120
    else
        R,G,B = 220,220,220
    end
    love.graphics.setColor(R,G,B)
    if math.cos(self.killingon) < 0 then
    love.graphics.draw(sprites["badasscellulecalm"].image,sprites["badasscellulecalm"][1], self.physicalbody:getX()-(128*(self.size / 80)), self.physicalbody:getY()-(128*(self.size / 80)), 0, self.size / 80, self.size / 80)
    else
    love.graphics.draw(sprites["badasscellule"].image,sprites["badasscellule"][1], self.physicalbody:getX()-(128*(self.size / 80)), self.physicalbody:getY()-(128*(self.size / 80)), 0, self.size / 80, self.size / 80)
    end
end

function CelluleBadass:kill(dt)
    self.killingon = self.killingon + dt
    if math.cos(self.killingon) > 0 then
        for k,l in pairs(Virus["list"]) do
            local dx, dy = math.abs(self.physicalbody:getX() - l.physicalbody:getX()), math.abs(self.physicalbody:getY() - l.physicalbody:getY())
            local d = dx*dx + dy*dy
            d = math.sqrt(d)
            if d < self.size + 80 then
                l.hp = l.hp - 1
            end
        end
    end
end


CelluleChiante = {size = 30, hp = 30}
setmetatable(CelluleChiante, Cellule)
Cellule.__index = CelluleChiante

function CelluleChiante:ral(dt)
    for k,l in pairs(Virus["list"]) do
        local dx, dy = math.abs(self.physicalbody:getX() - l.physicalbody:getX()), math.abs(self.physicalbody:getY() - l.physicalbody:getY())
        local d = dx*dx + dy*dy
        d = math.sqrt(d)
        if d < 360 then
            xv, yv = l.physicalbody:getLinearVelocity()
            l.physicalbody:applyForce(-xv*50,-yv*50)
        end
    end
end

function CelluleChiante:dessine()
    love.graphics.setColor(188,30,60)
    love.graphics.circle("fill", self.physicalbody:getX(), self.physicalbody:getY(), self.shape:getRadius())
    love.graphics.setColor(88,30,160,60)
    love.graphics.circle("fill", self.physicalbody:getX(), self.physicalbody:getY(), 360)
end

Celluletruelastboss= {size = 300, hp = 27000, killingon = 0, isboss = true, isfood = false}
setmetatable(Celluletruelastboss, Cellule)
Cellule.__index = Celluletruelastboss

function Celluletruelastboss:dessine()
    local R,G,B = 0,0,0
    if math.cos(self.killingon) > 0.0 then
        R,G,B = 220,220,140
    elseif math.cos(self.killingon) > 0.4 then
        R,G,B = 230,230,90
    elseif math.cos(self.killingon) > 0.65 then
        R,G,B = 240,240,60
    elseif math.cos(self.killingon) > 0.8 then
        R,G,B = 250,250,0
    else
        R,G,B = 220,220,220
    end
    love.graphics.setColor(R,G,B)
    love.graphics.circle("fill", self.physicalbody:getX(), self.physicalbody:getY(), self.shape:getRadius())
    local xrel, yrel = -math.cos(self.physicalbody:getAngle()), -math.sin(self.physicalbody:getAngle())
    local xm, ym = 128*(self.size / 80)*xrel - 128*(self.size / 80)*yrel, 128*(self.size / 80)*yrel + 128*(self.size / 80)*xrel 
    local a = self.physicalbody:getAngle()
    if math.cos(self.killingon) > 0.8 then
        love.graphics.draw(sprites["maccro"].image,sprites["maccro"][5], self.physicalbody:getX() + xm, self.physicalbody:getY() + ym, a, self.size / 80, self.size / 80)
    elseif math.cos(self.killingon) > 0.65 then
        love.graphics.draw(sprites["maccro"].image,sprites["maccro"][4], self.physicalbody:getX() + xm, self.physicalbody:getY() + ym, a, self.size / 80, self.size / 80)
    elseif math.cos(self.killingon) > 0.4 then
        love.graphics.draw(sprites["maccro"].image,sprites["maccro"][3], self.physicalbody:getX() + xm, self.physicalbody:getY() + ym, a, self.size / 80, self.size / 80)
    elseif math.cos(self.killingon) > 0.0 then
        love.graphics.draw(sprites["maccro"].image,sprites["maccro"][2], self.physicalbody:getX() + xm, self.physicalbody:getY() + ym, a, self.size / 80, self.size / 80)
    else
        love.graphics.draw(sprites["maccro"].image,sprites["maccro"][1], self.physicalbody:getX() + xm, self.physicalbody:getY() + ym, a, self.size / 80, self.size / 80)
    end
end

function Celluletruelastboss:kill(dt)
    self.killingon = self.killingon + dt
    if math.cos(self.killingon) > 0.8 then
        for k,l in pairs(Virus["list"]) do
            local dx, dy = math.abs(self.physicalbody:getX() - l.physicalbody:getX()), math.abs(self.physicalbody:getY() - l.physicalbody:getY())
            local d = dx*dx + dy*dy
            d = math.sqrt(d)
            if d < 1.7*self.size then
                local a = self.physicalbody:getAngle() + math.pi
                local dx, dy = (self.physicalbody:getX() - l.physicalbody:getX()), (self.physicalbody:getY() - l.physicalbody:getY())
                if (math.cos(a) * dx + math.sin(a) * dy ) > 0 then
                l.hp = l.hp - 1
                end
            end
        end
    end
end

function Celluletruelastboss:move(dt)
    local n = 0
    self.wantR, self.wantL, self.wantD, self.wantH = 0,0,0,0
    for k,l in pairs(Virus["list"]) do
    self.wantR = self.wantR + math.min(1,math.abs(math.max(0,l.physicalbody:getX()-self.physicalbody:getX())))
    self.wantL = self.wantL + math.min(1,math.abs(math.min(0,l.physicalbody:getX()-self.physicalbody:getX())))
    self.wantD = self.wantD + math.min(1,math.abs(math.max(0,l.physicalbody:getY()-self.physicalbody:getY())))
    self.wantH = self.wantH + math.min(1,math.abs(math.min(0,l.physicalbody:getY()-self.physicalbody:getY())))
    n = n + 1
    end
    if n == 0 then n = 1 end
    self.wantR = self.wantR/n
    self.wantL = self.wantL/n
    self.wantD = self.wantD/n
    self.wantH = self.wantH/n
    local xf, yf = self.wantR*dt - self.wantL*dt, self.wantD*dt - self.wantH*dt
    self.physicalbody:applyForce(xf*500*self.size*math.sqrt(time+100)/10,yf*500*self.size*math.sqrt(time+100)/10)
    local ax, ay= math.cos(self.physicalbody:getAngle()+math.pi/2 + math.cos(self.killingon/3)), math.sin(self.physicalbody:getAngle()+math.pi/2 + math.cos(self.killingon/3))
    if xf*ax + yf*ay > 0 then
    self.physicalbody:applyTorque(10000000)
    elseif xf*ax + yf*ay < 0 then
    self.physicalbody:applyTorque(-10000000)
    end
    local va = self.physicalbody:getAngularVelocity()
    self.physicalbody:applyTorque(-30000000*va)

    xv, yv = self.physicalbody:getLinearVelocity()
    self.physicalbody:applyForce(-xv*8,-yv*8)
end


