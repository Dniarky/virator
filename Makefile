#  Copyright 2016 Hugo Dupraz
#
#  This file is part of Virator.
#
#  Virator is free software. you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Virator is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Virator.  If not, see <http://www.gnu.org/licenses/>.

MAKEFLAGS += --no-builtin-rules

.PHONY: all binary clean distclean run help
.SUFFIXES:

TARGET = Viator.love
BIN_LNX = Viator
BIN_WIN = Viator.exe
BIN_OSX = Viator.app
LOVE_URL = https://bitbucket.org/rude/love/downloads/love-0.9.1-linux-src.tar.gz
LOVE_TARBALL = love-0.9.1-linux-src.tar.gz
LOVE_SRC = love-0.9.1

all: Viator.love

help:
	@echo "Run make without argument to create the game package ($(TARGET))."
	@echo ""
	@echo "Available targets:"
	@echo "  binary     Create game binary ($(BIN_LNX))."
	@echo "  clean      Remove game package and binaries."
	@echo "  distclean  Same as clean but also remove löve sources and binary."
	@echo "  run        Run the game."
	@echo "  love       Download and compile love."

binary: $(BIN_LNX)

run: $(BIN_LNX)
	./Viator

distclean: clean clean-love

clean:
	rm -f $(TARGET) $(BIN_LNX) $(BIN_WIN) $(BIN_OSX)

$(BIN_LNX): love $(TARGET)
	cat $^ > $@
	chmod +x $@

$(TARGET): $(shell find src -print) $(shell find graphics -print)
	rm -f $@
	cd src && zip -rX9 ../$@ .
	zip -rX9 $@ graphics

love: $(LOVE_SRC)/src/love
	cp $< $@

$(LOVE_SRC)/src/love: $(LOVE_SRC)
	( \
	  cd $(LOVE_SRC); \
	  ./configure --disable-shared; \
	  $(MAKE) -j6; \
	)

$(LOVE_SRC): $(LOVE_TARBALL)
	tar -xzf $<

$(LOVE_TARBALL):
	wget "$(LOVE_URL)" -O $@

clean-love:
	rm -f love
	rm -rf $(LOVE_SRC)
	rm -f $(LOVE_TARBALL)

optipng:
	du -hs graphics
	find graphics -iname '*.png' -print0 | xargs -0 -n1 -P6 -t optipng -o7 -quiet
	d
